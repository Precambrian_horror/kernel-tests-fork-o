# storage/block/bz2142432_direct_io_mapper_error

Storage: direct io device mapper errors on luks raid

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
